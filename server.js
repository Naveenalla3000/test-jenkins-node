import express from 'express';
const app = express();
import logger from './utils/logger.js';
import morgan from 'morgan';
const port = process.env.PORT || 8000;

const morganFormat = ':method :url :status :response-time ms';
app.use(morgan(morganFormat, {
  stream: {
    write: (message) => {
      const logObject = {
        method: message.split(' ')[0],
        url: message.split(' ')[1],
        status: message.split(' ')[2],
        responseTime: message.split(' ')[3],

      };
      logger.info(JSON.stringify(logObject));
    }
  }
}));

app.get("/", (req, res) => {
  return res.status(200).send({
    message: "Hello World!",
  });
});

app.listen(port, () => {
  console.log("Listening on " + port);
});

export default app;